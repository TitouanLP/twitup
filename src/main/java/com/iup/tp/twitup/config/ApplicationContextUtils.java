package com.iup.tp.twitup.config;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationContextUtils implements ApplicationContextAware {

  private static ApplicationContext CONTEXT;

  public static ApplicationContext getCONTEXT() {
    return CONTEXT;
  }

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    ApplicationContextUtils.CONTEXT = applicationContext;
  }
}
