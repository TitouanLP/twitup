package com.iup.tp.twitup.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.iup.tp.twitup.TwitupLauncher;

@Configuration
@ComponentScan(basePackageClasses = {TwitupLauncher.class})
public class AppConfiguration {
}
