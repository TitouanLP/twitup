package com.iup.tp.twitup.controllers.user;

import com.iup.tp.twitup.datamodel.User;

import java.util.Set;

public interface IUserController {

  Set<User> findAllUsers(String userTagFilter);
}
