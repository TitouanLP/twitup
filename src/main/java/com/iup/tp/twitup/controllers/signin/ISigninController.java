package com.iup.tp.twitup.controllers.signin;

import com.iup.tp.twitup.datamodel.User;

public interface ISigninController {
	
	/**
	 * Connecte un utilisateur dans le système avec
	 * ses identifiants.
	 * 
	 * @param tag Tag unique de l'utilisateur
	 * @param password Mot de passe de l'utilisateur
	 * @return <em>true</em> si la connexion est OK <em>false</em> sinon.
	 */
	User login(String tag, String password);
}
