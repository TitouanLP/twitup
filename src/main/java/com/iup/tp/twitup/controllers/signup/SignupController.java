package com.iup.tp.twitup.controllers.signup;

import org.springframework.stereotype.Component;

import com.iup.tp.twitup.controllers.BaseController;
import com.iup.tp.twitup.core.EntityManager;
import com.iup.tp.twitup.datamodel.IDatabase;
import com.iup.tp.twitup.datamodel.User;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;

@Component
public class SignupController extends BaseController implements ISignupController {

  public SignupController(IDatabase iDatabase, EntityManager entityManager) {
    super(iDatabase);
  }

  @Override
  public Map<String, String> validateUserData(String tag, String username, String password) {
    Map<String, String> errors = new HashMap<>();

    boolean tagValidation = this.database.getUsers().stream().anyMatch(u -> u.getUserTag().equals(tag));
    boolean tagValidationTaille = tag.trim().length() < 5;
    boolean usernameValidation = username.trim().length() < 5;
    boolean passwordValidation = password.trim().length() < 5;

    // Tag validation
    if (tagValidationTaille) errors.put("tag", "Le tag  doit au moins contenir 5 caractères");
    else if(tagValidation) errors.put("tag", "Le tag que vous avez choisi est déjà utilisé");

    // Username validation
    if (usernameValidation) errors.put("username", "Le nom d'utilisateur doit au moins contenir 5 caractères");

    // Password validation
    if (passwordValidation) errors.put("password", "Le mot de passe doit au moins contenir 5 caractères");

    return errors;
  }

  @Override
  public boolean registerUser(String tag, String username, String password, String avatar) {
    try {
      this.database.addUser(new User(UUID.randomUUID(), tag, password, username, new HashSet<>(), avatar));
      return true;
    } catch (Exception ignored) {
      return false;
    }
  }
}
