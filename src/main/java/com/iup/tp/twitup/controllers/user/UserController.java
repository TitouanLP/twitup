package com.iup.tp.twitup.controllers.user;

import org.springframework.stereotype.Component;

import com.iup.tp.twitup.controllers.BaseController;
import com.iup.tp.twitup.core.EntityManager;
import com.iup.tp.twitup.datamodel.IDatabase;
import com.iup.tp.twitup.datamodel.User;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UserController extends BaseController implements IUserController {

  private final EntityManager entityManager;

  public UserController(
    IDatabase iDatabase,
    EntityManager entityManager
  ) {
    super(iDatabase);
    this.entityManager = entityManager;
  }

  @Override
  public Set<User> findAllUsers(String searchedText) {
    Set<User> users = new HashSet<>(this.entityManager.getmDatabase().getUsers());
    if (!searchedText.isEmpty()) {
      return users
        .stream()
        .filter(user -> user.getUserTag().contains(searchedText))
        .collect(Collectors.toSet());
    }
    return users;
  }
}
