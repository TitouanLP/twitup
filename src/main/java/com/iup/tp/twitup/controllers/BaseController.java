package com.iup.tp.twitup.controllers;

import com.iup.tp.twitup.datamodel.IDatabase;

public abstract class BaseController {
  protected final IDatabase database;

  protected BaseController(IDatabase iDatabase) {
    this.database = iDatabase;
  }
}
