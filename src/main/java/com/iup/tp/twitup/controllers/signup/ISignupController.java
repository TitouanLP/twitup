package com.iup.tp.twitup.controllers.signup;

import java.util.Map;

public interface ISignupController {

  /**
   * Valide les informations saisie par l'utilisateur.
   *
   * @param tag Un tag unique qui n'existe pas en base de données.
   * @param username Nom d'utilisateur qui doit au minimum faire 5 caractères.
   * @param password Mot de passe qui doit au minimum faire 5 caractères.
   *
   * @return <em>true</em> si les données rentrées par l'utilisateur sont valides, <em>false</em> sinon.
   */
  Map<String, String> validateUserData(String tag, String username, String password);

  /**
   * Enregistre un utilisateur dans la base de données.
   *
   * @param tag Un tag unique dans la base de données.
   * @param username Le nom d'utilisateur.
   * @param password Le mot de passe de l'utilisateur.
   * @param avatar Un potentiel avatar de l'utilisateur.
   *
   * @return <em>true</em> si l'enregistrement en base s'est bien passé, <em>false</em> sinon.
   */
  boolean registerUser(String tag, String username, String password, String avatar);
}
