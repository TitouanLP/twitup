package com.iup.tp.twitup.controllers.twit;

import com.iup.tp.twitup.datamodel.Twit;
import com.iup.tp.twitup.model.UserConnectedModel;

import java.util.List;

public interface ITwitController {

  /**
   * Charge la liste des twits poster dans l'application
   *
   * @return La liste des
   */
  List<Twit> listTwit();

  Twit addNewTwit(String text, UserConnectedModel userConnectedModel);
}
