package com.iup.tp.twitup.controllers.signin;

import org.springframework.stereotype.Component;

import com.iup.tp.twitup.controllers.BaseController;
import com.iup.tp.twitup.datamodel.IDatabase;
import com.iup.tp.twitup.datamodel.User;

import java.util.Set;

@Component
public class SigninController extends BaseController implements ISigninController {

	public SigninController(IDatabase database) {
		super(database);
	}

	/**
	 * {@inheritDoc}
	 * @return
	 */
	@Override
	public User login(String tag, String password) {
		if (this.database == null) throw new RuntimeException("Impossible de se connecter à la base de données");
		Set<User> users = this.database.getUsers();
		return users
			.stream()
			.filter(user -> user.getUserTag().equals(tag) && user.getUserPassword().equals(password))
			.findFirst()
			.orElse(null);
	}

}
