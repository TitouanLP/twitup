package com.iup.tp.twitup.controllers.twit;

import org.springframework.stereotype.Component;

import com.iup.tp.twitup.controllers.BaseController;
import com.iup.tp.twitup.core.EntityManager;
import com.iup.tp.twitup.datamodel.IDatabase;
import com.iup.tp.twitup.datamodel.Twit;
import com.iup.tp.twitup.model.UserConnectedModel;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class TwitController extends BaseController implements ITwitController {

  private final EntityManager entityManager;

  public TwitController(
    IDatabase iDatabase,
    EntityManager entityManager
  ) {
    super(iDatabase);
    this.entityManager = entityManager;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<Twit> listTwit() {
    return new ArrayList<>(this.entityManager.getmDatabase().getTwits());
  }

  @Override
  public Twit addNewTwit(String text, UserConnectedModel userConnectedModel) {
    Twit twit = new Twit(UUID.randomUUID(), userConnectedModel.getConnectedUser(), System.currentTimeMillis(), text);
    this.entityManager.sendTwit(twit);
    return twit;
  }
}
