package com.iup.tp.twitup.events.app;

import org.springframework.context.ApplicationEvent;

public class UserSearchEvent extends ApplicationEvent {

  public UserSearchEvent(String source) {
    super(source);
  }

  @Override
  public String getSource() {
    return (String) this.source;
  }
}
