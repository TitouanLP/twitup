package com.iup.tp.twitup.events.app;

import org.springframework.context.ApplicationEvent;

public class AddNewFileEvent extends ApplicationEvent {

  public AddNewFileEvent(Object source) {
    super(source);
  }
}
