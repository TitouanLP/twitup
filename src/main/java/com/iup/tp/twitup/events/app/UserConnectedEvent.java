package com.iup.tp.twitup.events.app;

import org.springframework.context.ApplicationEvent;

import com.iup.tp.twitup.model.UserConnectedModel;

public class UserConnectedEvent extends ApplicationEvent {

  private final UserConnectedModel source;

  public UserConnectedEvent(UserConnectedModel source) {
    super(source);
    this.source = source;
  }

  @Override
  public UserConnectedModel getSource() {
    return source;
  }
}
