package com.iup.tp.twitup;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.iup.tp.twitup.config.AppConfiguration;
import com.iup.tp.twitup.core.Twitup;

import javax.swing.*;

/**
 * Classe de lancement de l'application.
 * 
 * @author S.Lucas
 */
public class TwitupLauncher {

	/**
	 * Launcher.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		new Twitup();
	}

}
