package com.iup.tp.twitup.routing;

public interface IRouterObserver {

  void notifyRedirect(String route);
}
