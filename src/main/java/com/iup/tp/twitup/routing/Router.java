package com.iup.tp.twitup.routing;

import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class Router implements IRouter {

  private Set<IRouterObserver> observerSet;

  {
    this.observerSet = new HashSet<>();
  }

  @Override
  public void addRouterObserver(IRouterObserver observer) {
    this.observerSet.add(observer);
  }

  @Override
  public void removeRouterObserver(IRouterObserver observer) {
    this.observerSet.remove(observer);
  }

  @Override
  public void redirect(String route) {
    this
      .observerSet
      .forEach(o -> o.notifyRedirect(route));
  }
}
