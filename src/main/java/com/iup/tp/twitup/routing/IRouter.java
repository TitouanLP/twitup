package com.iup.tp.twitup.routing;

public interface IRouter {

  /**
   * Ajoute un nouveau observer
   *
   * @param observer Observer à ajouté
   */
  void addRouterObserver(IRouterObserver observer);

  /**
   * Enlevé un  observer
   *
   * @param observer Observer à enlevé
   */
  void removeRouterObserver(IRouterObserver observer);

  /**
   * Notifie tout les observers qu'il y a eu une redirection
   */
  void redirect(String route);
}
