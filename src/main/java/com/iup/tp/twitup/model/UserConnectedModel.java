package com.iup.tp.twitup.model;

import com.iup.tp.twitup.datamodel.User;

public class UserConnectedModel {

  private final User connectedUser;

  private final boolean isConnected;

  public UserConnectedModel(User connectedUser, boolean isConnected) {
    this.connectedUser = connectedUser;
    this.isConnected = isConnected;
  }

  public User getConnectedUser() {
    return connectedUser;
  }

  public boolean isConnected() {
    return isConnected;
  }
}
