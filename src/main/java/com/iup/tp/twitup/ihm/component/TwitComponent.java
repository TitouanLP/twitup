package com.iup.tp.twitup.ihm.component;

import com.iup.tp.twitup.datamodel.Twit;

import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static java.util.Objects.requireNonNull;

public class TwitComponent extends JPanel {

  public TwitComponent(Twit twit) throws IOException {
    this.setLayout(new GridBagLayout());

    // Avatar twit
    InputStream defaultAvatar = getClass().getClassLoader().getResourceAsStream("resources/images/avatardefaut.png");
    InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream(twit.getTwiter().getAvatarPath());
    Icon avatarIcon = new ImageIcon(ImageIO.read(requireNonNull(resourceAsStream == null ? defaultAvatar : resourceAsStream)));
    JButton avatarButton = new JButton(avatarIcon);
    this.add(
      avatarButton,
      new GridBagConstraints(
        0, 0,
        1, 2,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(5, 5, 5, 5), 1, 1
      )
    );

    // Nom utilisateur twit
    JLabel nomUtilisateur = new JLabel(twit.getTwiter().getName());
    this.add(
      nomUtilisateur,
      new GridBagConstraints(
        0, 2,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(5, 5, 5, 5), 1, 1
      )
    );

    // Tag twit
    JLabel tag = new JLabel("@ " + twit.getTwiter().getUserTag() + "  " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm")));
    this.add(
      tag,
      new GridBagConstraints(
        1, 0,
        2, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(5, 5, 5, 5), 1, 1
      )
    );

    // Tag twit
    JLabel text = new JLabel(twit.getText());
    this.add(
      text,
      new GridBagConstraints(
        1, 1,
        2, 2,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(5, 5, 5, 5), 1, 1
      )
    );
  }
}
