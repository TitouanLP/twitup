package com.iup.tp.twitup.ihm.vue.toolbar;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.iup.tp.twitup.common.PropertiesManager;
import com.iup.tp.twitup.datamodel.User;
import com.iup.tp.twitup.events.app.UserConnectedEvent;
import com.iup.tp.twitup.ihm.vue.base.BaseVue;
import com.iup.tp.twitup.model.UserConnectedModel;
import com.iup.tp.twitup.routing.IRouter;

import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;

import static com.iup.tp.twitup.common.Constants.CONFIGURATION_FILE;
import static com.iup.tp.twitup.common.Constants.CONFIGURATION_KEY_RESOURCES_IMAGES_FRIEND_ICON;
import static com.iup.tp.twitup.common.Constants.CONFIGURATION_KEY_RESOURCES_IMAGES_HOME_ICON;
import static com.iup.tp.twitup.common.Constants.CONFIGURATION_KEY_RESOURCES_IMAGES_LOGOUT_ICON;
import static com.iup.tp.twitup.common.Constants.CONFIGURATION_KEY_RESOURCES_IMAGES_PROFIL_ICON;
import static com.iup.tp.twitup.common.Constants.CONFIGURATION_KEY_RESOURCES_IMAGES_SEARCH_ICON;
import static java.util.Objects.requireNonNull;

@Component
public class ToolBarVue extends BaseVue {

  private UserConnectedModel userConnectedModel;

  private final ApplicationEventPublisher applicationEventPublisher;

  private final IRouter router;

  private InputStream homeIconLocation;

  private InputStream profilIconLocation;

  private InputStream friendIconLocation;

  private InputStream searchIconLocation;

  private InputStream logoutIconLocation;

  public ToolBarVue(ApplicationEventPublisher applicationEventPublisher, IRouter router) {
    this.applicationEventPublisher = applicationEventPublisher;
    this.router = router;
  }

  private void initIcon() {
    this.homeIconLocation = requireNonNull(
      getClass()
        .getClassLoader()
        .getResourceAsStream(
          PropertiesManager
            .loadProperties(CONFIGURATION_FILE)
            .getProperty(CONFIGURATION_KEY_RESOURCES_IMAGES_HOME_ICON)
        )
    );
    this.profilIconLocation = requireNonNull(
      getClass()
        .getClassLoader()
        .getResourceAsStream(
          PropertiesManager
            .loadProperties(CONFIGURATION_FILE)
            .getProperty(CONFIGURATION_KEY_RESOURCES_IMAGES_PROFIL_ICON)
        )
    );
    this.friendIconLocation = requireNonNull(
      getClass()
        .getClassLoader()
        .getResourceAsStream(
          PropertiesManager
            .loadProperties(CONFIGURATION_FILE)
            .getProperty(CONFIGURATION_KEY_RESOURCES_IMAGES_FRIEND_ICON)
        )
    );
    this.searchIconLocation = requireNonNull(
      getClass()
        .getClassLoader()
        .getResourceAsStream(
          PropertiesManager
            .loadProperties(CONFIGURATION_FILE)
            .getProperty(CONFIGURATION_KEY_RESOURCES_IMAGES_SEARCH_ICON)
        )
    );
    this.logoutIconLocation = requireNonNull(
      getClass()
        .getClassLoader()
        .getResourceAsStream(
          PropertiesManager
            .loadProperties(CONFIGURATION_FILE)
            .getProperty(CONFIGURATION_KEY_RESOURCES_IMAGES_LOGOUT_ICON)
        )
    );
  }

  @Override
  protected void render() throws IOException {
    User user = this.userConnectedModel.getConnectedUser();

    // initialisation des icons
    this.initIcon();

    // Construction de la barre de menu
    this.add(
      this.buildNavBar(),
      new GridBagConstraints(
        0, 0,
        1, 1,
        1, 1,
        GridBagConstraints.NORTH, GridBagConstraints.BOTH,
        new Insets(5, 5, 5, 5), 1, 1
      )
    );
  }
  private JMenuBar buildNavBar() throws IOException {
    JMenuBar jMenuBar = new JMenuBar();
    jMenuBar.setBackground(Color.WHITE);
    jMenuBar.setLayout(new GridBagLayout());

    // Home
    Icon homeIcon = new ImageIcon(ImageIO.read(homeIconLocation));
    JButton homeButton = new JButton(homeIcon);
    homeButton.setBounds(30, 0, 0, 0);
    homeButton.setPreferredSize(new Dimension(50, 50));
    homeButton.addActionListener(e -> this.router.redirect("/twit"));
    jMenuBar.add(homeButton);

    // Profil
    Icon profilIcon = new ImageIcon(ImageIO.read(profilIconLocation));
    JButton profilButton = new JButton(profilIcon);
    profilButton.setBounds(30, 0, 0, 0);
    profilButton.setPreferredSize(new Dimension(50, 50));
    profilButton.addActionListener(e -> this.router.redirect("/profil"));
    jMenuBar.add(profilButton);

    // List Amis
    Icon friendIcon = new ImageIcon(ImageIO.read(friendIconLocation));
    JButton friendButton = new JButton(friendIcon);
    friendButton.setBounds(30, 0, 0, 0);
    friendButton.setPreferredSize(new Dimension(50, 50));
    friendButton.addActionListener(e -> this.router.redirect("/user-finder"));
    jMenuBar.add(friendButton);

    // Bare de recherche
    JTextField searchBar = new JTextField();
    searchBar.setPreferredSize(new Dimension(250, 20));
    searchBar.setBounds(70, 0, 0, 0);
    jMenuBar.add(searchBar);

    // Button action recherché
    Icon searchIcon = new ImageIcon(ImageIO.read(searchIconLocation));
    JButton searchButton = new JButton(searchIcon);
    searchButton.setBounds(30, 0, 0, 0);
    searchButton.setBounds(30, 0, 0, 0);
    searchButton.setPreferredSize(new Dimension(50, 50));
    jMenuBar.add(searchButton);

    //  Button de déconnexion
    Icon logoutIcon = new ImageIcon(ImageIO.read(logoutIconLocation));
    JButton logoutButton = new JButton(logoutIcon);
    logoutButton.setBackground(Color.WHITE);
    logoutButton.setBounds(30, 0, 0, 0);
    logoutButton.setBounds(30, 0, 0, 0);
    logoutButton.setPreferredSize(new Dimension(50, 50));
    logoutButton.addActionListener(e -> {
      applicationEventPublisher.publishEvent(new UserConnectedEvent(new UserConnectedModel(null, false)));
      this.router.redirect("/login");
    });
    jMenuBar.add(logoutButton);

    // Ajout du menu bar
    return jMenuBar;
  }

  @EventListener
  public void on(UserConnectedEvent userConnectedEvent) throws IOException {
    this.userConnectedModel = userConnectedEvent.getSource();
    if (this.userConnectedModel.isConnected() || this.userConnectedModel.getConnectedUser() != null) {
      this.userConnectedModel = userConnectedEvent.getSource();
      this.render();
    } else {
      this.removeAll();
      this.revalidate();
      this.repaint();
    }
  }
}
