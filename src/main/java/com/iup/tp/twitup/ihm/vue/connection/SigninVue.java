package com.iup.tp.twitup.ihm.vue.connection;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import com.iup.tp.twitup.annotations.Vue;
import com.iup.tp.twitup.controllers.signin.ISigninController;
import com.iup.tp.twitup.datamodel.User;
import com.iup.tp.twitup.events.app.UserConnectedEvent;
import com.iup.tp.twitup.ihm.vue.base.BaseVue;
import com.iup.tp.twitup.model.UserConnectedModel;
import com.iup.tp.twitup.routing.IRouter;

import javax.swing.*;

import java.awt.*;

@Vue(routeName = "/login")
@Component
public class SigninVue extends BaseVue {
  private final ISigninController signinController;

  private final IRouter router;

  private final ApplicationEventPublisher applicationEventPublisher;

  public SigninVue(
    ISigninController signinController,
    IRouter router,
    ApplicationEventPublisher applicationEventPublisher
  ) {
    this.signinController = signinController;
    this.router = router;
    this.applicationEventPublisher = applicationEventPublisher;

    // Affichage de la vue
    this.render();
  }

  @Override
  protected void render() {

    // Grid Bag Layout
    this.setLayout(new GridBagLayout());

    // Field de connection
    // title
    JLabel connexion = new JLabel("Tag : ");
    // champs de text
    JTextField champsText = new JTextField();

    this.add(
      connexion,
      new GridBagConstraints(
        0, 0,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
        new Insets(5, 5, 5, 5), 0, 0
      )
    );
    this.add(
      champsText,
      new GridBagConstraints(
        1, 0,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
        new Insets(5, 5, 5, 5), 200, 2
      )
    );

    // Field de mot de passe
    // title
    JLabel motDePasse = new JLabel("Mot de passe : ");
    // champs de text
    JPasswordField champsMotDePasse = new JPasswordField();

    this.add(
      motDePasse,
      new GridBagConstraints(
        0, 1,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
        new Insets(5, 5, 5, 5), 0, 0
      )
    );
    this.add(
      champsMotDePasse,
      new GridBagConstraints(
        1, 1,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
        new Insets(5, 5, 5, 5), 0, 0
      )
    );

    // Button de confirmation de connexion
    JButton post = new JButton("Connexion");
    this.add(
      post,
      new GridBagConstraints(
        1, 2,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
        new Insets(5, 5, 5, 5), 0, 0
      )
    );
    post.addActionListener(e -> {
      if (successConnection(this.signinController.login(champsText.getText(), new String(champsMotDePasse.getPassword())))) {
        champsMotDePasse.setText("");
        champsText.setText("");
      }
    });

    // Button d'inscription
    JButton inscription = new JButton("Inscription");
    this.add(
      inscription,
      new GridBagConstraints(
        0, 2,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
        new Insets(5, 5, 5, 5), 0, 0
      )
    );
    inscription.addActionListener(e -> this.router.redirect("/signup"));

    setBorder(BorderFactory.createTitledBorder("Connexion"));

  }

  private boolean successConnection(User connectedUser) {
    if (connectedUser != null) {
      applicationEventPublisher.publishEvent(new UserConnectedEvent(new UserConnectedModel(connectedUser, true)));
      this.router.redirect("/twit");
    }
    return connectedUser != null;
  }
}
