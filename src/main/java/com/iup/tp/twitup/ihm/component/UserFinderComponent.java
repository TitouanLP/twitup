package com.iup.tp.twitup.ihm.component;

import org.springframework.core.io.Resource;

import com.iup.tp.twitup.config.ApplicationContextUtils;

import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.*;
import java.io.File;
import java.io.IOException;

public class UserFinderComponent extends JPanel {

  public UserFinderComponent(
    String userAvatarPath,
    String userTag
  ) throws IOException {
    this.setLayout(new GridBagLayout());

    // Avatar
    Icon avatarIcon = null;
    if (userAvatarPath.contains("defaut")) {
      Resource resource = ApplicationContextUtils.getCONTEXT().getResource(userAvatarPath);
      avatarIcon = new ImageIcon(ImageIO.read(resource.getInputStream()));
    } else {
      File file = new File(userAvatarPath);
      avatarIcon = new ImageIcon(file.getAbsolutePath());
    }
    JLabel avatar = new JLabel(avatarIcon);
    this.add(
      avatar,
      new GridBagConstraints(
        0, 0,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(5, 5, 5, 5), 1, 1
      )
    );

    // tag
    JLabel tag = new JLabel("@" + userTag);
    this.add(
      tag,
      new GridBagConstraints(
        1, 0,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(5, 5, 5, 5), 1, 1
      )
    );
  }
}
