package com.iup.tp.twitup.ihm.vue.twit;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.iup.tp.twitup.annotations.Vue;
import com.iup.tp.twitup.controllers.twit.ITwitController;
import com.iup.tp.twitup.datamodel.Twit;
import com.iup.tp.twitup.events.app.AddNewFileEvent;
import com.iup.tp.twitup.ihm.component.TwitComponent;
import com.iup.tp.twitup.ihm.vue.base.BaseVue;
import com.iup.tp.twitup.routing.IRouter;

import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static java.util.Objects.requireNonNull;

@Vue(routeName = "/twit")
@Component
public class TwitVue extends BaseVue {

  private final ITwitController twitController;

  private final IRouter iRouter;

  public TwitVue(
    ITwitController twitController,
    IRouter iRouter
  ) {
    this.twitController = twitController;
    this.iRouter = iRouter;
  }

  @Override
  protected void render() throws IOException {
    // Grid Bag Layout
    this.setLayout(new GridBagLayout());
    this.setBounds(0, 0, 50, 50);

    // Scroll
    JPanel scrollArea = new JPanel();
    scrollArea.setLayout(new GridBagLayout());
    JScrollPane jScrollBar = new JScrollPane(scrollArea);
    jScrollBar.setLayout(new ScrollPaneLayout());
    jScrollBar.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

    // Button d'ajout d'un nouveau twit
    JButton addTwit = new JButton("Ajouter un nouveau twit");
    this.add(
      addTwit,
      new GridBagConstraints(
        0, 0,
        1, 1,
        1, 1,
        GridBagConstraints.NORTH, GridBagConstraints.NONE,
        new Insets(5, 5, 5, 5), 1, 1
      )
    );
    addTwit.addActionListener(e -> this.iRouter.redirect("/ajouter-twit"));

    // Construction des l'affiches des twits
    List<Twit> twits = this.twitController.listTwit();
    for (int i = 0; i < twits.size(); i++) {
      scrollArea.add(
        new TwitComponent(twits.get(i)),
        new GridBagConstraints(
          0, i,
          1, 1,
          1, 0,
          GridBagConstraints.CENTER, GridBagConstraints.NONE,
          new Insets(5, 5, 5, 5), 1, 1
        )
      );
    }

    this.add(
      jScrollBar,
      new GridBagConstraints(
        0, 1,
        500, 500,
        500, 500,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(5, 5, 5, 5), 1, 1
      )
    );
  }

  @EventListener
  public void on(AddNewFileEvent addNewFileEvent) throws IOException {
    this.removeAll();
    this.render();
    this.revalidate();
    this.repaint();
  }

}
