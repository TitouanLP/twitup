package com.iup.tp.twitup.ihm.vue.singup;

import org.springframework.stereotype.Component;

import com.iup.tp.twitup.annotations.Vue;
import com.iup.tp.twitup.common.PropertiesManager;
import com.iup.tp.twitup.controllers.signup.ISignupController;
import com.iup.tp.twitup.ihm.vue.base.BaseVue;
import com.iup.tp.twitup.routing.IRouter;

import javax.swing.*;

import java.awt.*;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import static com.iup.tp.twitup.common.Constants.CONFIGURATION_FILE;
import static com.iup.tp.twitup.common.Constants.CONFIGURATION_KEY_DEFAULT_AVATAR_ICON;
import static javax.swing.JFileChooser.FILES_ONLY;

@Vue(routeName = "/signup")
@Component()
public class SignupVue extends BaseVue {
  private final ISignupController signupController;

  private final IRouter iRouter;

  private final String defaultAvatarIcon;

  private final JLabel jUsernameError;
  private final JLabel jTagError;
  private final JLabel jPasswordError;

  {
    jUsernameError = new JLabel();
    jTagError = new JLabel();
    jPasswordError = new JLabel();
    defaultAvatarIcon = PropertiesManager.loadProperties(CONFIGURATION_FILE).getProperty(CONFIGURATION_KEY_DEFAULT_AVATAR_ICON);
  }

  public SignupVue(
    ISignupController signupController,
    IRouter iRouter
  ) {
    this.signupController = signupController;
    this.iRouter = iRouter;

    // affichage de la page
    this.render();
  }

  @Override
  protected void render() {

    // Grid Bag Layout
    this.setLayout(new GridBagLayout());

    // tag
    JLabel jTag = new JLabel("Tag : ");
    JTextField jTagField = new JTextField();
    this.add(
      jTag,
      new GridBagConstraints(
        0, 0,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(5, 5, 5, 5), 0, 0
      )
    );
    this.add(
      jTagField,
      new GridBagConstraints(
        1, 0,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(5, 5, 5, 5), 200, 2
      )
    );

    // nom
    JLabel jNom = new JLabel("Nom d'utilisateur : ");
    JTextField jNomField = new JTextField();
    this.add(
      jNom,
      new GridBagConstraints(
        0, 1,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(5, 5, 5, 5), 0, 0
      )
    );
    this.add(
      jNomField,
      new GridBagConstraints(
        1, 1,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(5, 5, 5, 5), 200, 2
      )
    );

    // mot de passe
    JLabel JMotDePasse = new JLabel("Mot de passe : ");
    JTextField jMotDePasseField = new JTextField();
    this.add(
      JMotDePasse,
      new GridBagConstraints(
        0, 2,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(5, 5, 5, 5), 0, 0
      )
    );
    this.add(
      jMotDePasseField,
      new GridBagConstraints(
        1, 2,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(5, 5, 5, 5), 200, 2
      )
    );

    // avatar
    AtomicReference<String> avatar = new AtomicReference<>("");
    JLabel JAvatar = new JLabel("Avatar : ");
    JButton jAvatarField = new JButton("Choisir une avatar");
    this.add(
      JAvatar,
      new GridBagConstraints(
        0, 3,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(5, 5, 5, 5), 0, 0
      )
    );
    this.add(
      jAvatarField,
      new GridBagConstraints(
        1, 3,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(5, 5, 5, 5), 200, 2
      )
    );
    jAvatarField.addActionListener(e -> {
      JFileChooser jFileChooser = new JFileChooser();
      jFileChooser.setFileSelectionMode(FILES_ONLY);
      if (jFileChooser.showOpenDialog(SignupVue.this) == JFileChooser.APPROVE_OPTION) {
        avatar.set(jFileChooser.getSelectedFile().getAbsolutePath());
      }
    });

    // Button annuler
    JButton jCancelButton = new JButton("Annuler");
    this.add(
      jCancelButton,
      new GridBagConstraints(
        0, 4,
        1, 1,
        1, 1,
        GridBagConstraints.WEST, GridBagConstraints.NONE,
        new Insets(5, 5, 5, 5), 0, 0
      )
    );
    jCancelButton.addActionListener(e -> this.iRouter.redirect("/login"));

    // Button créer
    JButton jConfirmButton = new JButton("Inscription");
    this.add(
      jConfirmButton,
      new GridBagConstraints(
        1, 4,
        1, 1,
        1, 1,
        GridBagConstraints.EAST, GridBagConstraints.NONE,
        new Insets(5, 5, 5, 5), 0, 0
      )
    );
    jConfirmButton.addActionListener(e -> {
      // Validation des données

      Map<String, String> isValid = this.signupController.validateUserData(jTagField.getText(), jNomField.getText(), jMotDePasseField.getText());

      if (!isValid.isEmpty()) {
        this.remove(jUsernameError);
        this.remove(jTagError);
        this.remove(jPasswordError);
        this.revalidate();
        this.repaint();

        if (isValid.containsKey("tag")) {
          this.jTagError.setText(isValid.get("tag"));
          this.add(
            jTagError,
            new GridBagConstraints(
              2, 0,
              1, 1,
              1, 1,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(5, 5, 5, 5), 0, 0
            )
          );
        }
        if (isValid.containsKey("username")) {
          this.jUsernameError.setText(isValid.get("username"));
          this.add(
            jUsernameError,
            new GridBagConstraints(
              2, 1,
              1, 1,
              1, 1,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(5, 5, 5, 5), 0, 0
            )
          );
        }
        if (isValid.containsKey("password")) {
          this.jPasswordError.setText(isValid.get("password"));
          this.add(
            jPasswordError,
            new GridBagConstraints(
              2, 2,
              1, 1,
              1, 1,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(5, 5, 5, 5), 0, 0
            )
          );
        }
        this.revalidate();
        this.repaint();
      }

      // Création de l'utilisateur
      if (isValid.isEmpty()) {
        String pathAvatar = avatar.get().isEmpty() ? this.defaultAvatarIcon : avatar.get();
        if (this.signupController.registerUser(jTagField.getText(), jNomField.getText(), jMotDePasseField.getText(), pathAvatar))
          this.iRouter.redirect("/login");
      }
    });

  }
}
