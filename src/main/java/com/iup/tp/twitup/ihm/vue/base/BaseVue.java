package com.iup.tp.twitup.ihm.vue.base;

import javax.swing.*;

import java.io.IOException;

public abstract class BaseVue extends JPanel{

  /**
   * Méthode commune entre toutes les vues pour afficher une contenue.
   */
  protected abstract void render() throws IOException, InterruptedException;
}
