package com.iup.tp.twitup.ihm;

import org.reflections.Reflections;

import com.iup.tp.twitup.annotations.Vue;
import com.iup.tp.twitup.common.PropertiesManager;
import com.iup.tp.twitup.config.ApplicationContextUtils;
import com.iup.tp.twitup.ihm.vue.base.BaseVue;
import com.iup.tp.twitup.ihm.vue.toolbar.ToolBarVue;
import com.iup.tp.twitup.routing.IRouter;
import com.iup.tp.twitup.routing.IRouterObserver;

import javax.swing.*;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.iup.tp.twitup.common.Constants.CONFIGURATION_FILE;
import static com.iup.tp.twitup.common.Constants.CONFIGURATION_KEY_ENTRY_VUE;
import static com.iup.tp.twitup.common.Constants.CONFIGURATION_KEY_RESOURCES_IMAGES_LOGO_50;
import static com.iup.tp.twitup.common.Constants.CONFIGURATION_KEY_RESOURCES_IMAGE_EDIT_ICON;
import static com.iup.tp.twitup.common.Constants.CONFIGURATION_KEY_RESOURCES_IMAGE_EXIT_ICON;
import static com.iup.tp.twitup.common.Constants.CONFIGURATION_KEY_VUES_PACKAGE;

/**
 * Classe de la vue principale de l'application.
 */
public class TwitupMainView implements IRouterObserver {

  private JFrame mFrame;

  private final Map<String, BaseVue> vues;

  private final Map<String, Object> router;

  private final String editIcon;

  private final String exitIcon;

  private final String logo;

  private final String vuesPackages;

  private final String entryVue;

  {
    this.editIcon = PropertiesManager.loadProperties(CONFIGURATION_FILE).getProperty(CONFIGURATION_KEY_RESOURCES_IMAGE_EDIT_ICON);
    this.exitIcon = PropertiesManager.loadProperties(CONFIGURATION_FILE).getProperty(CONFIGURATION_KEY_RESOURCES_IMAGE_EXIT_ICON);
    this.logo = PropertiesManager.loadProperties(CONFIGURATION_FILE).getProperty(CONFIGURATION_KEY_RESOURCES_IMAGES_LOGO_50);
    this.vuesPackages = PropertiesManager.loadProperties(CONFIGURATION_FILE).getProperty(CONFIGURATION_KEY_VUES_PACKAGE);
    this.entryVue = PropertiesManager.loadProperties(CONFIGURATION_FILE).getProperty(CONFIGURATION_KEY_ENTRY_VUE);
  }

  public TwitupMainView(IRouter iRouter) {
    // Initialisation des dépendences.
    iRouter.addRouterObserver(this);

    this.vues = this.initVues();

    // Initialisation du routeur
    iRouter.addRouterObserver(this);
    router = initRouting();
  }

  public void showGUI() {
    if (this.mFrame == null) {
      this.initGUI();
    }

    this.mFrame.setSize(1200, 1000);
    this.mFrame.setVisible(true);
  }

  private void initGUI() {
    this.mFrame = new JFrame("Twitter");
    this.mFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.mFrame.setLayout(new GridBagLayout());
    this.mFrame.setIconImage(new ImageIcon(getClass().getClassLoader().getResource(logo)).getImage());

    this.mFrame.setJMenuBar(this.buildMenuBar());
    this.notifyRedirect(this.entryVue);
  }

  @Override
  public void notifyRedirect(String route) {
    ToolBarVue toolBarVue = ApplicationContextUtils.getCONTEXT().getBean(ToolBarVue.class);
    Container contentPane = this.mFrame.getContentPane();
    Object o = this.router.get(route);
    contentPane.removeAll();
    contentPane.revalidate();
    contentPane.add(
      toolBarVue,
      new GridBagConstraints(
        0, 0,
        1, 1,
        1, 1,
        GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
        new Insets(5, 5, 5, 5), 1, 1
      )
    );
    if (o != null) {
      contentPane.add(
        (Component) o,
        new GridBagConstraints(
          0, 1,
          100, 100,
          100, 100,
          GridBagConstraints.NORTH, GridBagConstraints.BOTH,
          new Insets(5, 5, 5, 5), 1, 1
        )
      );
    }
    contentPane.repaint();
  }

  private Map<String, Object> initRouting() {
    final Map<String, Object> router;
    router = new HashMap<>(this.vues);
    return router;
  }

  /**
   * Création d'un menu
   */
  private JMenuBar buildMenuBar() {
    JMenuItem choixRepertoire = new JMenuItem("Choisir un fichier");
    choixRepertoire.addActionListener(e -> {
      JFileChooser chooser;
      chooser = new JFileChooser();
      chooser.setCurrentDirectory(new java.io.File("."));
      chooser.setDialogTitle("Choisir le répertoire d'échange");
      chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
      if (chooser.showOpenDialog(TwitupMainView.this.mFrame) == JFileChooser.APPROVE_OPTION) {
        System.out.println("getCurrentDirectory(): " + chooser.getCurrentDirectory());
        System.out.println("getSelectedFile() : " + chooser.getSelectedFile());
      } else {
        System.out.println("No Selection ");
      }
    });

    JMenuItem quitter = new JMenuItem("Quitter", new ImageIcon(getClass().getClassLoader().getResource(exitIcon)));
    quitter.addActionListener(e -> System.exit(0));

    JMenu fichier = new JMenu("Fichier");
    fichier.add(choixRepertoire);
    fichier.add(quitter);

    JMenuItem aPropos = new JMenuItem("A propos", new ImageIcon(getClass().getClassLoader().getResource(editIcon)));
    aPropos.addActionListener(e -> {
      new JOptionPane();
      JOptionPane.showMessageDialog(
        null,
        "UBO M2-TIIL \n Département Informatique",
        "A propos",
        JOptionPane.INFORMATION_MESSAGE,
        new ImageIcon(getClass().getClassLoader().getResource(editIcon))
      );
    });

    JMenu pointInterrogation = new JMenu("?");
    pointInterrogation.add(aPropos);


    JMenuBar jMenuBar = new JMenuBar();
    jMenuBar.add(fichier);
    jMenuBar.add(pointInterrogation);
    return jMenuBar;
  }


  private Map<String, BaseVue> initVues() {
    Map<String, BaseVue> returnValue = new HashMap<>();
    Reflections reflections = new Reflections(this.vuesPackages);
    Set<Class<? extends BaseVue>> vues = reflections.getSubTypesOf(BaseVue.class);
    vues
      .stream()
      .filter(vue -> vue.isAnnotationPresent(Vue.class))
      .forEach(vue -> {
        returnValue.put(vue.getAnnotation(Vue.class).routeName(), ApplicationContextUtils.getCONTEXT().getBean(vue));
      });
    return returnValue;
  }

  public JFrame getmFrame() {
    return mFrame;
  }
}
