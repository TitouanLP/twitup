package com.iup.tp.twitup.ihm.vue.twit;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.iup.tp.twitup.annotations.Vue;
import com.iup.tp.twitup.controllers.twit.ITwitController;
import com.iup.tp.twitup.events.app.UserConnectedEvent;
import com.iup.tp.twitup.ihm.vue.base.BaseVue;
import com.iup.tp.twitup.model.UserConnectedModel;
import com.iup.tp.twitup.routing.IRouter;

import javax.swing.*;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

@Component
@Vue(routeName = "/ajouter-twit")
public class AddTwitVue extends BaseVue {

  public static final int LIMIT_CHAR_TWIT = 249;

  private final IRouter router;

  private final ITwitController twitController;

  private int limitCharacterTwit;

  private UserConnectedModel userConnectedModel;

  public AddTwitVue(IRouter router, ITwitController twitController) {
    this.router = router;
    this.twitController = twitController;
    this.setLayout(new GridBagLayout());
  }

  @Override
  protected void render() {

    // zone de text
    JTextArea jTextArea = new JTextArea(100, 100);
    jTextArea.setLineWrap(true);
    jTextArea.addKeyListener(new KeyAdapter() {
      @Override
      public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
          System.out.println("Retour arriere");
          --limitCharacterTwit;
          jTextArea.setEditable(true);
        } else if (limitCharacterTwit <= LIMIT_CHAR_TWIT) {
          limitCharacterTwit++;
        }
        if (limitCharacterTwit > LIMIT_CHAR_TWIT) {
          jTextArea.setEditable(false);
        }
        System.out.println(limitCharacterTwit);
      }
    });
    this.add(
      jTextArea,
      new GridBagConstraints(
        0, 0,
        2, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(5, 5, 5, 5), 1, 1
      )
    );

    // Button Annuler
    JButton annuler = new JButton("Annuler");
    annuler.addActionListener(e -> this.router.redirect("/twit"));
    this.add(
      annuler,
      new GridBagConstraints(
        0, 1,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(5, 5, 5, 5), 1, 1
      )
    );

    // Button Publier
    JButton publier = new JButton("Publier");
    publier.addActionListener(e -> {
      twitController.addNewTwit(jTextArea.getText(), userConnectedModel);
      jTextArea.setText("");
      this.router.redirect("/twit");
    });
    this.add(
      publier,
      new GridBagConstraints(
        1, 1,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(5, 5, 5, 5), 1, 1
      )
    );
  }

  @EventListener
  public void on(UserConnectedEvent userConnectedEvent) {
    this.userConnectedModel = userConnectedEvent.getSource();
    this.removeAll();
    this.render();
    this.revalidate();
  }
}
