package com.iup.tp.twitup.ihm.vue.people;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.iup.tp.twitup.annotations.Vue;
import com.iup.tp.twitup.config.ApplicationContextUtils;
import com.iup.tp.twitup.controllers.user.IUserController;
import com.iup.tp.twitup.datamodel.User;
import com.iup.tp.twitup.events.app.AddNewFileEvent;
import com.iup.tp.twitup.events.app.UserSearchEvent;
import com.iup.tp.twitup.ihm.component.UserFinderComponent;
import com.iup.tp.twitup.ihm.component.UserSearchComponent;
import com.iup.tp.twitup.ihm.vue.base.BaseVue;
import com.iup.tp.twitup.routing.IRouter;

import javax.swing.*;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

@Component
@Vue(routeName = "/user-finder")
public class UsersFinderVue extends BaseVue {

  private final IUserController userController;

  private final IRouter router;

  private String searchedText;

  private JScrollPane usersPanel;
  private Set<User> allUsers;

  public UsersFinderVue(
    IUserController userController,
    IRouter router
  ) {
    this.userController = userController;
    this.router = router;
    this.searchedText = "";

    allUsers = new HashSet<>();

    this.usersPanel = new JScrollPane();
    this.add(usersPanel);

    this.setLayout(new GridBagLayout());

    this.searchBar();
  }

  @Override
  protected void render() throws IOException {
    // utilisateurs
    JPanel scrollArea = new JPanel();
    scrollArea.setLayout(new GridBagLayout());
    allUsers = this.userController.findAllUsers(this.searchedText);
    usersPanel = new JScrollPane(scrollArea);
    usersPanel.setLayout(new ScrollPaneLayout());
    usersPanel.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
    usersPanel.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    int i = 0;
    for (User user : allUsers) {
      scrollArea.add(
        new UserFinderComponent(user.getAvatarPath(), user.getUserTag()),
        new GridBagConstraints(
          0, i,
          1, 1,
          1, 1,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(5, 5, 5, 5), 1, 1
        )
      );
      i++;
    }

    this.add(
      usersPanel,
      new GridBagConstraints(
        0, 1,
        100, 100,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(5, 5, 5, 5), 1, 1
      )
    );
  }

  private void searchBar() {
    // bar de recherche
    UserSearchComponent comp = new UserSearchComponent(ApplicationContextUtils.getCONTEXT(), this.searchedText);
    comp.requestFocus();
    comp.setVisible(true);
    this.add(
      comp,
      new GridBagConstraints(
        0, 0,
        1, 1,
        1, 0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(5, 5, 5, 5), 1, 1
      )
    );
  }

  @EventListener
  public void on(AddNewFileEvent addNewFileEvent) throws IOException {
    this.remove(usersPanel);
    this.render();
    this.revalidate();
    this.repaint();
  }

  @EventListener
  public void on(UserSearchEvent userSearchEvent) throws IOException {
    this.searchedText = userSearchEvent.getSource();
    this.remove(usersPanel);
    this.render();
    this.revalidate();
    this.repaint();
  }
}
