package com.iup.tp.twitup.ihm.component;

import org.springframework.context.ApplicationEventPublisher;

import com.iup.tp.twitup.events.app.UserSearchEvent;

import javax.swing.*;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class UserSearchComponent extends JTextField {

  private final ApplicationEventPublisher applicationEventPublisher;

  public UserSearchComponent(ApplicationEventPublisher applicationEventPublisher, String text) {
    this.applicationEventPublisher = applicationEventPublisher;
    // bar de recherche
    this.setPreferredSize(new Dimension(250, 20));
    this.setText(text);
    this.setBounds(70, 0, 0, 0);
    this.addKeyListener(new KeyAdapter() {

      @Override
      public void keyReleased(KeyEvent e) {
        applicationEventPublisher.publishEvent(new UserSearchEvent(UserSearchComponent.this.getText()));
      }
    });
  }
}
