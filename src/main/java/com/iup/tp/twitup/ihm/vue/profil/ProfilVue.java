package com.iup.tp.twitup.ihm.vue.profil;

import org.springframework.context.event.EventListener;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.iup.tp.twitup.annotations.Vue;
import com.iup.tp.twitup.config.ApplicationContextUtils;
import com.iup.tp.twitup.events.app.UserConnectedEvent;
import com.iup.tp.twitup.ihm.vue.base.BaseVue;
import com.iup.tp.twitup.model.UserConnectedModel;
import com.iup.tp.twitup.routing.IRouter;

import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.*;
import java.io.IOException;

@Vue(routeName = "/profil")
@Component
public class ProfilVue extends BaseVue {


  private UserConnectedModel userConnectedModel;

  private final IRouter router;

  public ProfilVue(
    IRouter router
  ) {
    this.router = router;
  }

  @Override
  protected void render() throws IOException {
    if (userConnectedModel == null || userConnectedModel.getConnectedUser() == null) {
      this.router.redirect("/login");
      return;
    }

    Resource resourceAvatarImage = ApplicationContextUtils.getCONTEXT().getResource(userConnectedModel.getConnectedUser().getAvatarPath());
    this.setLayout(new GridBagLayout());

    // Avatar
    Icon imageAvatar = new ImageIcon(ImageIO.read(resourceAvatarImage.getInputStream()));
    JLabel avatar = new JLabel(imageAvatar);
    this.add(
      avatar,
      new GridBagConstraints(
        0, 0,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(5, 5, 5, 5), 1, 1
      )
    );

    // Tag
    JLabel tag = new JLabel("@" + this.userConnectedModel.getConnectedUser().getUserTag());
    this.add(
      tag,
      new GridBagConstraints(
        0, 1,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(5, 5, 5, 5), 1, 1
      )
    );

    // nom
    JLabel nom = new JLabel(this.userConnectedModel.getConnectedUser().getName());
    this.add(
      nom,
      new GridBagConstraints(
        0, 2,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(5, 5, 5, 5), 1, 1
      )
    );

    // nom abonnement
    JLabel nombreAbonnement = new JLabel("Mes Abonnements: " + this.userConnectedModel.getConnectedUser().getFollows().size());
    this.add(
      nombreAbonnement,
      new GridBagConstraints(
        0, 3,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(5, 5, 5, 5), 1, 1
      )
    );

    // nom abonnement
    JLabel nombreAbonnee = new JLabel("Mes Abonnées: " + this.userConnectedModel.getConnectedUser().getFollows().size());
    this.add(
      nombreAbonnee,
      new GridBagConstraints(
        1, 3,
        1, 1,
        1, 1,
        GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(5, 5, 5, 5), 1, 1
      )
    );
  }

  @EventListener
  public void on(UserConnectedEvent userConnectedEvent) throws IOException {
    this.userConnectedModel = userConnectedEvent.getSource();
    if (this.userConnectedModel.getConnectedUser() != null && this.userConnectedModel.isConnected())
      this.render();
    else {
      this.removeAll();
      this.revalidate();
      this.repaint();
    }
  }
}
