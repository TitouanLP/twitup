package com.iup.tp.twitup.core;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.io.Resource;

import com.iup.tp.twitup.common.Constants;
import com.iup.tp.twitup.common.PropertiesManager;
import com.iup.tp.twitup.config.AppConfiguration;
import com.iup.tp.twitup.config.ApplicationContextUtils;
import com.iup.tp.twitup.datamodel.DatabaseObserver;
import com.iup.tp.twitup.datamodel.IDatabase;
import com.iup.tp.twitup.events.file.IWatchableDirectory;
import com.iup.tp.twitup.events.file.WatchableDirectory;
import com.iup.tp.twitup.ihm.TwitupMainView;
import com.iup.tp.twitup.ihm.TwitupMock;
import com.iup.tp.twitup.routing.IRouter;

import javax.swing.*;

import java.io.File;
import java.io.IOException;

/**
 * Classe principale l'application.
 *
 * @author S.Lucas
 */
public class Twitup {
  private final AnnotationConfigApplicationContext context;

  /**
   * Base de données.
   */
  protected IDatabase mDatabase;

  /**
   * Gestionnaire des entités contenu de la base de données.
   */
  protected EntityManager mEntityManager;

  /**
   * Vue principale de l'application.
   */
  protected TwitupMainView mMainView;

  /**
   * Classe de surveillance de répertoire
   */
  protected IWatchableDirectory mWatchableDirectory;

  /**
   * Répertoire d'échange de l'application.
   */
  protected String mExchangeDirectoryPath;

  /**
   * Idnique si le mode bouchoné est activé.
   */
  protected boolean mIsMockEnabled =
    Boolean.parseBoolean(PropertiesManager.loadProperties(Constants.CONFIGURATION_FILE).getProperty(Constants.CONFIGURATION_KEY_MOCK_ENABLED));

  /**
   * Nom de la classe de l'UI.
   */
  protected String mUiClassName;

  /**
   * Constructeur.
   */
  public Twitup() {
    this.context = new AnnotationConfigApplicationContext(AppConfiguration.class);

    // Init du look and feel de l'application
    this.initLookAndFeel();

    // Initialisation de la base de données
    this.initDatabase();

    if (this.mIsMockEnabled) {
      // Initialisation du bouchon de travail
      this.initMock();
    }

    // Initialisation de l'IHM
    this.initGui();

    // Initialisation du répertoire d'échange
    this.initDirectory();
  }

  /**
   * Initialisation du look and feel de l'application.
   */
  protected void initLookAndFeel() {
  }

  /**
   * Initialisation de l'interface graphique.
   */
  protected void initGui() {
    // this.mMainView...
    this.mMainView = new TwitupMainView(ApplicationContextUtils.getCONTEXT().getBean(IRouter.class));
    mMainView.showGUI();
  }

  /**
   * Initialisation du répertoire d'échange (depuis la conf ou depuis un file
   * chooser). <br/>
   * <b>Le chemin doit obligatoirement avoir été saisi et être valide avant de
   * pouvoir utiliser l'application</b>
   */
  protected void initDirectory() {
    // TODO: SI nous somme en dev charger le dossier database SINON laisser choisir l'utilisateur
    Resource database = ApplicationContextUtils.getCONTEXT().getResource("database");
    JFileChooser jFileChooser = new JFileChooser();
    jFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    if (jFileChooser.showOpenDialog(mMainView.getmFrame()) == JFileChooser.APPROVE_OPTION) {
//    try {
      this.initDirectory(jFileChooser.getSelectedFile().getAbsolutePath());
//    } catch (IOException e) {
//      e.printStackTrace();
//    }
    }
  }

  /**
   * Indique si le fichier donné est valide pour servire de répertoire
   * d'échange
   *
   * @param directory , Répertoire à tester.
   */
  protected boolean isValideExchangeDirectory(File directory) {
    // Valide si répertoire disponible en lecture et écriture
    return directory != null && directory.exists() && directory.isDirectory() && directory.canRead()
      && directory.canWrite();
  }

  /**
   * Initialisation du mode bouchoné de l'application
   */
  protected void initMock() {
    TwitupMock mock = new TwitupMock(ApplicationContextUtils.getCONTEXT().getBean(IDatabase.class), ApplicationContextUtils.getCONTEXT().getBean(EntityManager.class));
    mock.showGUI();
  }

  /**
   * Initialisation de la base de données
   */
  protected void initDatabase() {
    mDatabase = ApplicationContextUtils.getCONTEXT().getBean(IDatabase.class);
    mEntityManager = ApplicationContextUtils.getCONTEXT().getBean(EntityManager.class);
    mDatabase.addObserver(new DatabaseObserver(mEntityManager));
  }

  /**
   * Initialisation du répertoire d'échange.
   *
   * @param directoryPath
   */
  public void initDirectory(String directoryPath) {
    mExchangeDirectoryPath = directoryPath;
    mWatchableDirectory = new WatchableDirectory(directoryPath);
    mEntityManager.setExchangeDirectory(directoryPath);

    mWatchableDirectory.initWatching();
    mWatchableDirectory.addObserver(mEntityManager);
  }

  public void show() {
    // ... setVisible?
  }
}
