package com.iup.tp.twitup.datamodel;

import com.iup.tp.twitup.core.EntityManager;

public class DatabaseObserver implements IDatabaseObserver {

	private final EntityManager entityManager;

	public DatabaseObserver(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public void notifyTwitAdded(Twit addedTwit) {
		entityManager.sendTwit(addedTwit);
	}

	@Override
	public void notifyTwitDeleted(Twit deletedTwit) {

	}

	@Override
	public void notifyTwitModified(Twit modifiedTwit) {
		System.out.println(modifiedTwit);

	}

	@Override
	public void notifyUserAdded(User addedUser) {
		this.entityManager.sendUser(addedUser);

	}

	@Override
	public void notifyUserDeleted(User deletedUser) {
		System.out.println(deletedUser);

	}

	@Override
	public void notifyUserModified(User modifiedUser) {
		System.out.println(modifiedUser);
	}

}
